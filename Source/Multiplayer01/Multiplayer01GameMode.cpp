// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Multiplayer01GameMode.h"
#include "Multiplayer01Character.h"
#include "UObject/ConstructorHelpers.h"

AMultiplayer01GameMode::AMultiplayer01GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
