// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Multiplayer01GameMode.generated.h"

UCLASS(minimalapi)
class AMultiplayer01GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMultiplayer01GameMode();
};



