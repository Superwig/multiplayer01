// Fill out your copyright notice in the Description page of Project Settings.

#include "MovingPlatform.h"

FTarget* AMovingPlatform::Begin() { return Locations.GetData(); }
FTarget* AMovingPlatform::End() { return Locations.GetData(); }
const FTarget* AMovingPlatform::cBegin() const { return Locations.GetData(); }
const FTarget* AMovingPlatform::cEnd() const { return Locations.GetData() + Locations.Num(); }

AMovingPlatform::AMovingPlatform()
{
	SetMobility(EComponentMobility::Movable);
	PrimaryActorTick.bCanEverTick = true;
	bReplicateMovement = true;
	bReplicates = true;
	
	RotateOption = FFinishedOption::Cycle;
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	if (!bLoop)
	{
		RotateOption = FFinishedOption::Cycle;
	}
	if (Curve && Locations.Num() && HasAuthority())
	{
		for (auto&& Target : Locations)
		{
			Target.Location = GetTransform().TransformPosition(Target.Location);
		}
		//Add the initial location to the front of the array
		Locations.EmplaceAt(0, FTarget(GetActorLocation(), Time));
		InitialLocation = GetActorLocation();
		//set the first target
		TargetInfo = &Locations[1];

		FOnTimelineFloat ProgFunc;
		ProgFunc.BindUFunction(this, TEXT("MovePlatform"));
		MoveTimeline.AddInterpFloat(Curve, ProgFunc);
		MoveTimeline.SetPlayRate(1 / TargetInfo->Time);

		FOnTimelineEvent OnFinish;
		OnFinish.BindUFunction(this, TEXT("NextTarget"));
		MoveTimeline.SetTimelineFinishedFunc(OnFinish);

		MoveTimeline.PlayFromStart();
	}
}

void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveTimeline.TickTimeline(DeltaTime);
}

void AMovingPlatform::MovePlatform(float Delta)
{
	const FVector TargetDelta = TargetInfo->Location;
	const float TargetTime = TargetInfo->Time;
	MoveTimeline.SetPlayRate(1 / TargetTime);
	FVector Location = FMath::Lerp(InitialLocation, TargetDelta, Delta);
	SetActorLocation(Location);
}

void AMovingPlatform::NextTarget()
{
	InitialLocation = GetActorLocation();
	switch (RotateOption)
	{
	case FFinishedOption::Cycle:
		//iterate the array
		if (TargetInfo++ != cEnd() - 1)
		{
			MoveTimeline.PlayFromStart();
			return;
		}
		//if end of array set the next location to the starting location
		if (bLoop)
		{
			TargetInfo = Begin();
			MoveTimeline.PlayFromStart();
			return;
		}
		break;
	case FFinishedOption::Reverse:
		//forwards
		if (!bReverse)
		{
			if (++TargetInfo != cEnd())
			{
				MoveTimeline.PlayFromStart();
				return;
			}
			//iterate back to Begin
			--TargetInfo;
			bReverse = true;
		}
		if (bReverse)
		{
			if (TargetInfo-- != cBegin())
			{
				MoveTimeline.PlayFromStart();
				return;
			}
			bReverse = false;
			//reset to first target
			TargetInfo = &Locations[1];
			MoveTimeline.PlayFromStart();
		}
		break;
	}
}
